package com.tripapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;

@SpringBootApplication
public class TripApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(TripApiApplication.class, args);
	}

}
