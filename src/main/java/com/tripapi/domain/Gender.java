package com.tripapi.domain;

/**
 * @author Murad Zulfugarov
 */

public enum Gender {
    MALE, FEMALE
}
