package com.tripapi.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * @author Murad Zulfugarov
 */

@Data
@Entity
@Table(name = "users")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @NotBlank(message = "invalid_name")
    @Column(name = "name", nullable = false)
    private String name;

    @NotBlank(message = "invalid_surname")
    @Column(name = "surname", nullable = false)
    private String surname;

    @NotBlank(message = "phone_required")
    @Column(name = "phone", nullable = false, unique = true)
    private String phone;

    @Column(name = "gender")
    private Gender gender;

    @JsonIgnore
    @Size(min = 6, message = "invalid_password")
    @Column(name = "password", nullable = false)
    private String password;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "user_role",
            joinColumns = @JoinColumn(name = "user_id",
                    referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "role_id",
                    referencedColumnName = "id"))
    private List<Role> roles;

    @Transient
    private Token token;
}
