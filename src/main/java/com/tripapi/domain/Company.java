package com.tripapi.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.List;

@Data
@Entity
@Table(name = "companies")
public class Company {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @NotBlank(message = "invalid_name")
    @Column(name = "name", nullable = false)
    private String name;

    @NotBlank(message = "phone_required")
    @Column(name = "phone", nullable = false, unique = true)
    private String phone;

    @NotBlank(message = "email_required")
    @Column(name = "email", nullable = false, unique = true)
    @Email
    private String email;

    @Column(name = "location")
    private String location;

    @Column(name = "social_media")
    private String social_media;

    @JsonIgnore
    @Size(min = 6, message = "invalid_password")
    @Column(name = "password", nullable = false)
    private String password;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "company_role",
            joinColumns = @JoinColumn(name = "company_id",
                    referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "role_id",
                    referencedColumnName = "id"))
    private List<Role> roles;

    @Transient
    private Token token;
}
