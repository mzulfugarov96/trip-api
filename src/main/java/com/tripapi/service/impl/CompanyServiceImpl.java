package com.tripapi.service.impl;

import com.tripapi.domain.Company;
import com.tripapi.exception.UserAlreadyExistsException;
import com.tripapi.repository.CompanyRepository;
import com.tripapi.service.CompanyService;
import com.tripapi.util.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class CompanyServiceImpl implements CompanyService {
    @Autowired
    private CompanyRepository companyRepository;
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public Company register(Company company) {
        Company dbCompany = companyRepository.findByPhone(company.getPhone());
        if (dbCompany != null) {
            throw new UserAlreadyExistsException(Message.USER_ALREADY_EXISTS);
        }
        String password = company.getPassword();
        String encodedPwd = bCryptPasswordEncoder.encode(password);
        company.setPassword(encodedPwd);
        return companyRepository.save(company);
    }
}
