package com.tripapi.service.impl;

import com.tripapi.domain.User;
import com.tripapi.exception.UserAlreadyExistsException;
import com.tripapi.repository.UserRepository;
import com.tripapi.service.UserService;
import com.tripapi.util.Message;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

/**
 * @author Murad Zulfugarov
 */

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    UserRepository userRepository;
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public User register(User user) {
        User dbUser = userRepository.findByPhone(user.getPhone());
        if (dbUser != null) {
            throw new UserAlreadyExistsException(Message.USER_ALREADY_EXISTS);
        }
        String password = user.getPassword();
        String encodedPwd = bCryptPasswordEncoder.encode(password);
        user.setPassword(encodedPwd);
        return userRepository.save(user);
    }

}