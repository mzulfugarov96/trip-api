package com.tripapi.service;

import com.tripapi.domain.User;

/**
 * @author Murad Zulfugarov
 */

public interface UserService {
    User register(User user);
}
