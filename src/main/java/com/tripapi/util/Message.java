package com.tripapi.util;

/**
 * @author Murad Zulfugarov
 */

public interface Message {
    String USER_ALREADY_EXISTS = "user_already_exists";
    String UNAUTHORIZED = "unauthorized";

}
