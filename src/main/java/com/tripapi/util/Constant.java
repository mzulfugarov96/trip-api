package com.tripapi.util;

/**
 * @author Murad Zulfugarov
 */

public interface Constant {
    String GRANT_TYPE = "grant_type";
    String USERNAME = "username";
    String PASSWORD = "password";
    String AUTHORIZATION = "Authorization";
}
