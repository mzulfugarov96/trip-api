package com.tripapi.exception;

/**
 * @author Murad Zulfugarov
 */

public class UserAlreadyExistsException extends RuntimeException {
    public UserAlreadyExistsException(String errorMessage) {
        super(errorMessage);
    }

}
