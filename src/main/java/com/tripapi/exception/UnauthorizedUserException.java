package com.tripapi.exception;

/**
 * @author Murad Zulfugarov
 */

public class UnauthorizedUserException extends RuntimeException {
    public UnauthorizedUserException(String message) {
        super(message);
    }
}
