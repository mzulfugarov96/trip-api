package com.tripapi.exception;

import com.tripapi.domain.ErrorResponse;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import org.springframework.web.util.WebUtils;

import java.util.stream.Collectors;

/**
 * @author Murad Zulfugarov
 */

@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {
    @ExceptionHandler({UserAlreadyExistsException.class, UnauthorizedUserException.class})
    public final ResponseEntity<ErrorResponse> handleRuntimeException(Exception ex, WebRequest request) {
        HttpHeaders headers = new HttpHeaders();
        if (ex instanceof UserAlreadyExistsException) {
            HttpStatus status = HttpStatus.CONFLICT;
            UserAlreadyExistsException alreadyExistsException = (UserAlreadyExistsException) ex;
            return handleCustomException(alreadyExistsException, headers, status, request);
        } else if (ex instanceof UnauthorizedUserException) {
            HttpStatus status = HttpStatus.UNAUTHORIZED;
            UnauthorizedUserException unauthorizedUserException = (UnauthorizedUserException) ex;
            return handleCustomException(unauthorizedUserException, headers, status, request);
        }
        return null;
    }

    /**
     * Customize the response for Runtime exception.
     */
    protected ResponseEntity<ErrorResponse> handleCustomException(RuntimeException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        ErrorResponse response = new ErrorResponse(ex.getMessage(), status.value());
        return handleExceptionInternal(ex, response, headers, status, request);
    }

    /**
     * Customize the response of Hibernate Validator
     */
    protected ResponseEntity<ErrorResponse> handleExceptionInternal(Exception ex, ErrorResponse body, HttpHeaders headers, HttpStatus status, WebRequest request) {
        if (HttpStatus.INTERNAL_SERVER_ERROR.equals(status)) {
            request.setAttribute(WebUtils.ERROR_EXCEPTION_ATTRIBUTE, ex, WebRequest.SCOPE_REQUEST);
        }
        return new ResponseEntity<>(body, headers, status);
    }

    @Override
    protected ResponseEntity<Object> handleBindException(BindException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        String message = ex.getBindingResult().getFieldErrors().stream()
                .map(DefaultMessageSourceResolvable::getDefaultMessage).collect(Collectors.joining("; "));
        ErrorResponse response = new ErrorResponse(message, status.value());
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }
}
