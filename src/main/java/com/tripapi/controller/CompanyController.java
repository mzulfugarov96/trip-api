package com.tripapi.controller;

import com.tripapi.domain.Company;
import com.tripapi.service.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/company")
public class CompanyController {
    @Autowired
    CompanyService companyService;

    @PostMapping("")
    public ResponseEntity<Company> register(@Valid Company company) {
        return new ResponseEntity<>(companyService.register(company), HttpStatus.CREATED);
    }

}
